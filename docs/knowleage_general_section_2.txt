npm init -y
---> tự khởi tạo dự án luôn không cần điền các thông tin

file server.js
---> khởi động nodejs, không viết Middleware
---> không đụng đến nữa
---> chỉ sửa trong file app.js

folder utils
---> chứa các hàm, class, tính năng chúng ta thường hay sử dụng

folder configs
---> khác với file env

node -v
---> xem version của node
---> xem cách up version của node trong Anonystick

npm i express --save

file package-lock.json
---> tracking các file đã cài

node_modules .env
---> add vào .gitignore