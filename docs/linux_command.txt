ls
---> xem danh sách

mkdir tên_folder
---> tạo mới tên_folder

cd tên_folder
---> đi vào tên_folder

touch tên_file
---> tạo mới file trong folder

touch đường_dẫn/tên_file
---> tạo mới tên_file trong đường_dẫn

mv tên_file_now tên_file_new
---> đổi tên từ tên_file_now sang tên_file_new

mv "tên folder có khoảng trắng" "tên_folder_không_khoảng_trắng"
---> nếu có lỗi: Device or resource busy 
---> chạy sudo mv hoặc run as Administrator

cp đường_dẫn_file_source đường_dẫn_file_destination
---> copy nội dung từ đường_dẫn_file_source sang đường_dẫn_file_destination

readlink -f tên_file
---> lấy đường dẫn tuyệt đối của tên file


https://funix.edu.vn/chia-se-kien-thuc/cach-su-dung-lenh-de-xoa-thu-muc-tep-trong-linux/